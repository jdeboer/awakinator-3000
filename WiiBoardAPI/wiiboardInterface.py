import logging
import matplotlib.pyplot as plt
import time
import threading
import numpy as np


from WiiBoardAPI.wiiboard import discover, Wiiboard

class WiiBoardInterface(Wiiboard):
    def __init__(self,address,ignore_n=100):
        Wiiboard.__init__(self,address)
        self.x = 0
        self.y = 0
        self.total_mass = 0
        self.n = 0
        self.ignore_n = ignore_n
        self.has_jumped = False
        self.last_masses = [0]*10
        self.mass_counter = 0
    #this gets called continously by the runner thread, updating the x and y locations of the center of mass
    #it also updates the total mass on the wiiboard.
    def on_mass(self, mass):
        top_right = mass['top_right']
        top_left = mass['top_left']
        bottom_left = mass['bottom_left']
        bottom_right = mass['bottom_right']
        #add all the regions for total mass
        self.total_mass = top_right+top_left+bottom_left+bottom_right

        #cycle the mass values we saw last
        if(self.mass_counter<10):
            self.last_masses[self.mass_counter]=self.total_mass
            self.mass_counter = self.mass_counter+1
        else:
            self.mass_counter = 0
        #see if the user jumped
        self.calc_jumped()
        self.x = ((top_right+bottom_right)/2) - ((top_left+bottom_left)/2)
        self.y = ((top_right+top_left)/2) - ((bottom_left+bottom_right)/2)

    def calc_jumped(self):
        for i in range(len(self.last_masses)-1):
            next = self.last_masses[i+1]
            prev = self.last_masses[i]
            diff = next-prev
            if(diff > 40):
                self.has_jumped = True
                return
            self.has_jumped = False

    def get_center_mass_coordinates(self):
        return self.x,self.y
    def get_lean_direction(self):
        if self.x < -10:
            return "WEST"
        elif self.x > 10:
            return "EAST"
        elif  self.y > 10:
            return "NORTH"
        elif self.y < -10:
            return "SOUTH"
        else: return "CENTER"
        
    def get_has_jumped(self):
        return self.has_jumped
    def get_total_mass(self):
        return self.total_mass
           
        
def connect():
    #find a wiiboard
    wiiboards = discover(duration=6)
    print("Found wiiboards: %s", str(wiiboards))
    if not wiiboards:
        raise Exception("Press the red sync button on the board")
    address = wiiboards[0]
    
    #here we connect
    board = WiiBoardInterface(address)

    thread = threading.Thread(target=board.loop)
    thread.start()

    return board