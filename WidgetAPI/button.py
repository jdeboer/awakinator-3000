from MatrixAPI.samplebase import SampleBase
from rgbmatrix import graphics
from MatrixAPI import drawhelper

class Button():
    def __init__(self,x,y,text,width,font,onclick):
        self.x = x
        self.y = y
        self.text = text
        self.width = width
        self.selected = False
        self.font = font
        self.onclick = onclick

        #FSM data
        self.NorthT,self.SouthT,self.EastT,self.WestT = self,self,self,self


    def draw_button(self,canvas):
        if self.selected:
            drawhelper.draw_rect(canvas,self.x,self.y,self.x+self.width,self.y+6,graphics.Color(0,0,255))
        else: drawhelper.draw_rect(canvas,self.x,self.y,self.x+self.width,self.y+6,graphics.Color(255,255,255))
        graphics.DrawText(canvas,self.font,self.x+1,self.y+6,graphics.Color(255,0,255),self.text)
    
    def click_button(self):
        self.onclick()
    def get_y(self):
        return self.y
    def get_x(self):
        return self.x
    #set FSM states
    def set_FSM(self,NorthT,SouthT,EastT,WestT):
        self.NorthT,self.SouthT,self.EastT,self.WestT = NorthT,SouthT,EastT,WestT