from WidgetAPI.button import Button

SUCCESIVE_INPUTS = 5

class Menu():
    def __init__(self, buttons, selected_button):
        self.buttons = buttons
        self.selected_button = selected_button
        self.selected_button.selected = True
        self.succesiveInputCount = 0
        self.lastInput = ""

    def input(self,direction):
        # Make it necessary to input the same direction SUCCESIVE_INPUTS times in order to react
        # Otherwise the menu is way too overresponsive
        if direction != self.lastInput:
            self.lastInput = direction
            self.succesiveInputCount = 1
            return
        else:
            self.succesiveInputCount += 1
            if self.succesiveInputCount < SUCCESIVE_INPUTS:
                return
        
        if direction == "NORTH":
            self.selected_button.selected = False
            self.selected_button = self.selected_button.NorthT
            self.selected_button.selected = True
        if direction == "SOUTH":
            self.selected_button.selected = False
            self.selected_button = self.selected_button.SouthT
            self.selected_button.selected = True  
        if direction == "EAST":
            self.selected_button.selected = False
            self.selected_button = self.selected_button.EastT
            self.selected_button.selected = True
        if direction == "WEST":
            self.selected_button.selected = False
            self.selected_button = self.selected_button.WestT
            self.selected_button.selected = True

    def click(self):
        return self.selected_button.click_button()
    def draw_buttons(self,canvas):
        for button in self.buttons:
            button.draw_button(canvas)