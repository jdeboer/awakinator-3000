from MatrixAPI.samplebase import SampleBase
from rgbmatrix import graphics
import threading
import time

from WidgetAPI.apis import *
from Games.snake import Snake
from Games.snake import Cell
from MatrixAPI import drawhelper
from WidgetAPI.menu import Menu
from WidgetAPI.button import Button
import datetime

class Widget():
  def __init__(self,x,y,waittime,color):
    self.font = graphics.Font()
    self.font.LoadFont("/home/pi/rpi-rgb-led-matrix/fonts/4x6.bdf")
    self.thread = threading.Thread(target=self.fetch_data)
    self.textToDraw = "?"
    self.x = x
    self.y = y
    self.waittime =waittime
    self.color = color
  def draw_widget(self,canvas):
     graphics.DrawText(canvas, self.font, self.x, self.y, self.color, self.textToDraw)
  def fetch_data(self):
    pass

class DayWidget(Widget):
  def __init__(self,x,y,waittime,color):
    super().__init__(x,y,waittime,color)
    self.thread.start()
  def fetch_data(self):
    while True:
      self.textToDraw = currentdayoftheweek()
      time.sleep(self.waittime)

class TimeWidget(Widget):
  def __init__(self,x,y,waittime,color):
    super().__init__(x,y,waittime,color)
    self.thread.start()
  def fetch_data(self):
    while True:
      self.textToDraw = currenttimedate()
      time.sleep(self.waittime)

class TempWidget(Widget):
  def __init__(self,x,y,waittime,color):
    super().__init__(x,y,waittime,color)
    self.thread.start()
  def fetch_data(self):
    while True:
      tmp = temperature()
      if tmp < 0:
        self.color = graphics.Color(255,255,0)
      elif tmp < 25:
        self.color = graphics.Color(124,252,0)
      elif tmp >= 25:
        self.color = graphics.Color(255,0,0)
      self.textToDraw = str(tmp)
      time.sleep(self.waittime)

class StockWidget(Widget):
  def __init__(self,x,y,waittime,color):
    super().__init__(x,y,waittime,color)
    self.thread.start()
    self.stockText = "?"

  def fetch_data(self):
    while True:
      self.stockText =stockprice()
      self.textToDraw = self.stockText[1:]
      time.sleep(self.waittime)

  def draw_widget(self,canvas):
    if(self.stockText[0]=="+"):
      graphics.DrawText(canvas, self.font, self.x, self.y, graphics.Color(0,255,0), self.textToDraw)
    else: graphics.DrawText(canvas, self.font, self.x, self.y, graphics.Color(255,0,0), self.textToDraw)

#screen that lets you go to the settings widget and the snake widget
class MainScreenWidget():

  def playSnake(self):
    self.show_screen = False
    self.screen.main_interface = SnakeWidget(self.interface,self.screen)
    
  def goToSetting(self):
    self.show_screen = False
    self.screen.main_interface = SettingsWidget(self.interface,self.screen)
    
  def __init__(self,wiiboardInterface):
    self.font = graphics.Font()
    self.font.LoadFont("/home/pi/rpi-rgb-led-matrix/fonts/4x6.bdf")
    self.thread = threading.Thread(target=self.main_loop)
    self.waittime = 0.5
    self.interface = wiiboardInterface
    self.dir = "CENTER"
    self.jump = False
    self.screen = None
    self.show_screen = True

    #build menus with buttons
    button1 = Button(10,10,"SNAKE",20,self.font,self.playSnake)
    button2 = Button(10,18,"SETTINGS",32,self.font,self.goToSetting)
    button1.set_FSM(button1,button2,button1,button1)
    button2.set_FSM(button1,button2,button2,button2)
    self.menu = Menu([button1,button2],button1)

    self.thread.start()

  def set_screen(self,screen):
    self.screen = screen
  def draw_widget(self,canvas):
   self.menu.draw_buttons(canvas)
    
  #get user input from board and do stuff with it
  def main_loop(self):
    time.sleep(0.5)
    while self.show_screen:
        self.jump = self.interface.get_has_jumped()
        self.dir = self.interface.get_lean_direction()
        if self.jump:
           self.menu.click()
        else: self.menu.input(self.dir)
        time.sleep(self.waittime)


class SettingsWidget():

  def hourPlusF(self):
    self.hours  = (self.hours+1)%24
    self.current_set_time = datetime.time(self.hours,self.minutes)
  def hourMinusF(self):
    self.hours  = (self.hours-1)%24
    self.current_set_time = datetime.time(self.hours,self.minutes)
  def minutePlusF(self):
    self.minutes  = (self.minutes+1)%60
    self.current_set_time = datetime.time(self.hours,self.minutes)
  def minuteMinusF(self):
    self.minutes  = (self.minutes-1)%60
    self.current_set_time = datetime.time(self.hours,self.minutes)

  def done(self):
    self.show_setting = False
    self.screen.alarm.set_alarm(self.current_set_time)
    self.mainS = MainScreenWidget(self.interface)
    self.mainS.set_screen(self.screen)
    self.screen.main_interface = self.mainS
  def toggle(self):
    self.screen.alarm.toggle_alarm()
    time.sleep(0.5)

  def __init__(self,wiiboardInterface,screen):
    self.font = graphics.Font()
    self.font.LoadFont("/home/pi/rpi-rgb-led-matrix/fonts/4x6.bdf")
    self.thread = threading.Thread(target=self.main_loop)
    self.waittime = 0.3
    self.interface = wiiboardInterface
    self.dir = "CENTER"
    self.jump = False
    self.screen = screen
    self.hours = 0
    self.minutes = 0
    self.show_setting = True

    self.current_set_time = datetime.time(self.hours,self.minutes)

    x_offset = 0
    y_offset = 8

    #build clock menu
    hourPlus = Button(x_offset+3,y_offset+1,"+",4,self.font,self.hourPlusF)
    hourMinus = Button(x_offset+3,y_offset+16,"-",4,self.font,self.hourMinusF)
    minutePlus = Button(x_offset+11,y_offset+1,"+",4,self.font,self.minutePlusF)
    minuteMinus = Button(x_offset+11,y_offset+16,"-",4,self.font,self.minuteMinusF)
    toggleButton = Button(x_offset+22,y_offset+3,"TOGGLE",24,self.font,self.toggle)
    doneButton = Button(x_offset+22,y_offset+12,"DONE",18,self.font,self.done)

    #build menu FSM
    hourPlus.set_FSM(hourPlus,hourMinus,minutePlus,hourPlus)
    hourMinus.set_FSM(hourPlus,hourMinus,minuteMinus,hourMinus)
    minutePlus.set_FSM(minutePlus,minuteMinus,toggleButton,hourPlus)
    minuteMinus.set_FSM(minutePlus,minuteMinus,doneButton,hourMinus)
    toggleButton.set_FSM(toggleButton,doneButton,toggleButton,minutePlus)
    doneButton.set_FSM(toggleButton,doneButton,doneButton,minuteMinus)

    self.menu = Menu([hourPlus,hourMinus,minutePlus,minuteMinus,toggleButton,doneButton],hourMinus)

    self.thread.start()


  def draw_widget(self,canvas):
    #render buttons
    self.menu.draw_buttons(canvas)
    #render time
    graphics.DrawText(canvas, self.font, 2, 22, graphics.Color(255,0,255),str(self.current_set_time)[:-3])
    
  #get user input from board and do stuff with it
  def main_loop(self):
    time.sleep(self.waittime)
    while self.show_setting:
        self.jump = self.interface.get_has_jumped()
        self.dir = self.interface.get_lean_direction()
        if self.jump:
          self.menu.click()
        else: self.menu.input(self.dir)
        time.sleep(self.waittime)

class SnakeWidget():
  def __init__(self,wiiboardInterface,screen):
    self.font = graphics.Font()
    self.font.LoadFont("/home/pi/rpi-rgb-led-matrix/fonts/4x6.bdf")
    self.thread = threading.Thread(target=self.main_loop)
    self.waittime = 0.2
    self.interface = wiiboardInterface
    self.dir = "CENTER"
    self.snekGame = Snake(24,11,3)
    self.running = "?"
    self.screen = screen
    self.thread.start()
   
  def draw_widget(self,canvas):
    board = self.snekGame.get_board()
    x_stride = 0
    y_stride = 0
    for y,row in enumerate(board):
      x_stride = 0
      for x,item in enumerate(row):
        if(item==Cell.EMPTY):
          drawhelper.draw_2x2_pixel(canvas,(x+1+x_stride),(y+9+y_stride),100,100,0)
        if(item==Cell.SNAKE):
          drawhelper.draw_2x2_pixel(canvas,(x+1+x_stride),(y+9+y_stride),0,255,0)
        if(item==Cell.CANDY):
          drawhelper.draw_2x2_pixel(canvas,(x+1+x_stride),(y+9+y_stride),0,0,255)
        x_stride = x_stride+1
      y_stride+=1

    #draw end game messages
    if self.running == "VICTORY":
        graphics.DrawText(canvas,self.font,3,20,graphics.Color(255,255,255),"YOU WON :)")
    if self.running == "DIED":
        graphics.DrawText(canvas,self.font,3,20,graphics.Color(255,255,255),"YOU DIED :(")

    #draw score
    graphics.DrawText(canvas,self.font,51,31,graphics.Color(255,255,255),str(self.snekGame.get_score()))
  #get user input from board and do stuff with it
  def main_loop(self):
    self.running = "RUNNING"
    while not self.running=="VICTORY":

      if(self.running == "RUNNING"):
          dir = self.interface.get_lean_direction()
          #snake moving code
          if(dir=="NORTH"):
            self.snekGame.setDirectionNorth()
          elif(dir=="SOUTH"):
            self.snekGame.setDirectionSouth()
          elif(dir=="EAST"):
            self.snekGame.setDirectionEast()
          elif(dir=="WEST"):
            self.snekGame.setDirectionWest()
          else: pass
          self.running = self.snekGame.update()
      if(self.running == "VICTORY"):
        self.snekGame.clear()
        time.sleep(5)
        self.screen.alarm.stop_alarm()
        self.mainS = MainScreenWidget(self.interface)
        self.mainS.set_screen(self.screen)
        self.screen.main_interface = self.mainS
      if(self.running == "DIED"):
        self.snekGame.clear()
        time.sleep(5)
        self.snekGame.reset_field()
        self.running = "RUNNING"
      time.sleep(self.waittime) 
    #the game is done
