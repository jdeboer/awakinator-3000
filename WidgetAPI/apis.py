from datetime import date
import calendar
import requests
import datetime
import requests
from bs4 import BeautifulSoup


def currentdayoftheweek():
    my_date = date.today()
    return calendar.day_name[my_date.weekday()][0:3].lower()

def temperature():
    api_address = 'http://api.openweathermap.org/data/2.5/weather?q=Nijmegen&appid=b97b2b1cb098752336f2a2ee0758070f'
    json_data = requests.get(api_address).json()
    mn = json_data.get('main')
    temperature = mn.get('temp')
    return round(temperature-273.15)

def currenttimedate():
    time = (str(datetime.datetime.now().time()))[0:5]
    date = list(((str(datetime.datetime.now().date()))[5:10]))
    date[0], date[3] = date[3], date[0]
    date[1], date[4] = date[4], date[1]
    date = "".join(date)
    return (time + " " + date)

def stockprice():
    url = 'https://finance.yahoo.com/quote/%5EAEX?p=^AEX&.tsrc=fin-srch'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')
    price = list(str(soup.find_all('div', {'class':'My(6px) Pos(r) smartphone_Mt(6px)'})[0]))
    result = []
    for i in range(0,len(price)):
        if price[i] == '%':
            result = ("".join(price[i-5:i-1]))
    return result