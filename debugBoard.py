import time

from WiiBoardAPI.wiiboardInterface import connect


def get_lean_direction(x,y):
        if x < -10:
            return "WEST"
        elif x > 10:
            return "EAST"
        elif  y > 10:
            return "NORTH"
        elif y < -10:
            return "SOUTH"
        else: return "CENTER"

board = connect()
while True:
  x,y = board.get_center_mass_coordinates()
  print(x,y)
  print(get_lean_direction(x,y))
  time.sleep(0.1)