import random
from enum import Enum

class Direction(Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3

class Cell(Enum):
    EMPTY = 0
    SNAKE = 1
    CANDY = 2

class Snake():
    def __init__(self, width=10, height=10,req_score=5):
        self.height = height
        self.width = width
        self.x = int(self.width/2)
        self.y = int(self.height/2)
        self.tail = [(self.y,self.x)]
        self.board = [[Cell.EMPTY for col in range(self.width)] for row in range(self.height)]
        self.board[self.y][self.x] = Cell.SNAKE
        self.__addCandy()
        self.direction = Direction.NORTH
        self.score = 0
        self.req_score = req_score
    
    def clear(self):
        self.board = [[Cell.EMPTY for col in range(self.width)] for row in range(self.height)]
    def reset_field(self):
        self.x = int(self.width/2)
        self.y = int(self.height/2)
        self.tail = [(self.y,self.x)]
        self.board = [[Cell.EMPTY for col in range(self.width)] for row in range(self.height)]
        self.board[self.y][self.x] = Cell.SNAKE
        self.__addCandy()
        self.direction = Direction.NORTH
        self.score = 0


    def get_score(self):
        return self.score

    def get_board(self):
        return self.board

    def setDirectionNorth(self):
        if self.direction != Direction.SOUTH:
            self.direction = Direction.NORTH

    def setDirectionSouth(self):
        if self.direction != Direction.NORTH:
            self.direction = Direction.SOUTH

    def setDirectionWest(self):
        if self.direction != Direction.EAST:
            self.direction = Direction.WEST

    def setDirectionEast(self):
        if self.direction != Direction.WEST:
            self.direction = Direction.EAST

    def __addCandy(self):
        x = random.randint(0,self.width - 1)
        y = random.randint(0,self.height - 1)
        if (self.board[y][x] == Cell.EMPTY):
            self.board[y][x] = Cell.CANDY
        else:
            self.__addCandy()
    
def update(self):
        if (self.direction == Direction.NORTH):
            self.y -= 1
        elif (self.direction == Direction.SOUTH):
            self.y += 1
        elif (self.direction == Direction.EAST):
            self.x += 1
        elif (self.direction == Direction.WEST):
            self.x -= 1

        
        if (self.x >= self.width or self.y >= self.height or self.x < 0 or self.y < 0 or
            self.board[self.y][self.x] == Cell.SNAKE):
            #you died boy       
            return "DIED"
        elif (self.board[self.y][self.x] == Cell.EMPTY):
            lastTail = self.tail.pop(0)
            self.board[lastTail[0]][lastTail[1]] = Cell.EMPTY
        elif (self.board[self.y][self.x] == Cell.CANDY):
            self.__addCandy()
            self.score+=1


        #check if you won
        if self.score == self.req_score:
            return "VICTORY"

        self.board[self.y][self.x] = Cell.SNAKE
        self.tail.append((self.y,self.x))
        return "RUNNING"