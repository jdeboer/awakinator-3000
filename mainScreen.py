import vlc
from MatrixAPI.samplebase import SampleBase
from rgbmatrix import graphics
from WidgetAPI.apis import *
from SoundStuff.soundManager import soundManager
from WidgetAPI.widgets import *
from WiiBoardAPI.wiiboardInterface import connect
from SoundStuff.Alarm import Alarm
from MatrixAPI import drawhelper
import time
import threading
import os




class Screen(SampleBase):
  def __init__(self,main_interface, widget_top, widget_right1, widget_right2, widget_right3,wiiBoard):
    # in our design, widget_top is time and date, widget_right1 is day of the week, widget_right2 = temperature, widget_right3=stocks
    super(Screen, self)
    self.main_interface = main_interface
    self.widget_top = widget_top
    self.widget_right1 = widget_right1
    self.widget_right2 = widget_right2
    self.widget_right3 = widget_right3
    self.line_color = graphics.Color(255,255,255)
    self.wiiBoard =  wiiBoard
    self.alarm_going = False
    self.alarm = Alarm(self,self.wiiBoard)


  def run(self):
    offscreen_canvas = self.matrix.CreateFrameCanvas()
    font = graphics.Font()
    font.LoadFont("/home/pi/rpi-rgb-led-matrix/fonts/4x6.bdf") # 4x6 for 4 by 6 pixels font
    self.main_interface.set_screen(self)

    while True:
      offscreen_canvas.Clear()
      #main widget
      self.main_interface.draw_widget(offscreen_canvas)
      #time
      self.widget_top.draw_widget(offscreen_canvas)
      #day
      self.widget_right1.draw_widget(offscreen_canvas)
      #temp
      self.widget_right2.draw_widget(offscreen_canvas)
      #aex stock
      self.widget_right3.draw_widget(offscreen_canvas)

      #draw box around main widget box
      drawhelper.draw_rect(offscreen_canvas,0,8,49,31,self.line_color)

      if self.alarm.alarm_on:
        drawhelper.draw_2x2_pixel(offscreen_canvas,20,1,255,0,0)
      #update screen
      offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)

  

if __name__ == "__main__":
  board = connect()

  screen = Screen(MainScreenWidget(board), TimeWidget(1,6,0.1,graphics.Color(0,0,255)),
           DayWidget(52,6,1,graphics.Color(0,248,255)), TempWidget(52,13,60,graphics.Color(0,0,255)), 
           StockWidget(52,20,1,graphics.Color(0,248,255)), board)

  if (not screen.process()):
   pass
