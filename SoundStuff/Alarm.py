import datetime
import time
import threading

from SoundStuff.soundManager import soundManager
from WidgetAPI.widgets import *

class Alarm:
    def __init__(self,screen,wiiboard):
        self.sound_manager = soundManager()
        self.screen = screen
        self.alarm_on = False
        self.wiiboard = wiiboard
        #default alarm time for now, will change later
        self.alarm_time = datetime.time(23,21,0)

        self.thread = threading.Thread(target=self.alarm_poll)
        self.thread.start()

    #polls to see if the alarm should go of, check for a window of about 1 second after the alarm
    def alarm_poll(self):
        while True:
            if self.alarm_on:
              #fetch current time like HH:MM:SS:MMMMM
              current_time = datetime.datetime.now().time()
              #check if the minute and hour match, this is probably accurate enough
              if current_time.hour == self.alarm_time.hour and current_time.minute == self.alarm_time.minute:
                if not self.screen.alarm_going: self.start_alarm()
            time.sleep(1)

    #accepts a datime time object
    def set_alarm(self,time):
        self.alarm_time = time
        print(str(time))


    #able to toggle the alarm on and off at will
    def set_on(self):
        self.alarm_on = True
    def set_off(self):
        self.alarm_on = False
    def toggle_alarm(self):
         self.alarm_on = not self.alarm_on
    def start_alarm(self):
        #start alarm stuff
        self.screen.main_interface = SnakeWidget(self.wiiboard,self.screen)
        self.screen.alarm_going = True
        self.sound_manager.play_alarm_sound()

    def stop_alarm(self):
        #stop alarm stuff
        self.screen.alarm_going = False
        self.sound_manager.play_sound = False

