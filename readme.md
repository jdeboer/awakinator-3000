Awakinator 3000
===================
Awakinator 3000 in action:

https://www.youtube.com/watch?v=HsQ0i5N0Qq0


Installation 
-------
- Install RPI-RGB-LED-Matrix api  https://github.com/hzeller/rpi-rgb-led-matrix
- Install the necessary python libraries

Run ``mainScreen.py``.

If the installation does not work, we can provide the original image file that was used to develop the project.
<!-- The image file for awakinator can be given by us, it is 15GB large so it is hard for us to link to.  -->