from MatrixAPI.samplebase import SampleBase
from rgbmatrix import graphics

def draw_rect(canvas,x,y,x1,y2,color):
    graphics.DrawLine(canvas,x,y,x,y2,color)
    graphics.DrawLine(canvas,x,y,x1,y,color)
    graphics.DrawLine(canvas,x1,y,x1,y2,color)
    graphics.DrawLine(canvas,x,y2,x1,y2,color)

def draw_2x2_pixel(canvas,x,y,r,g,b):
    canvas.SetPixel(x,y,r,g,b)
    canvas.SetPixel(x+1,y,r,g,b)
    canvas.SetPixel(x,y+1,r,g,b)    
    canvas.SetPixel(x+1,y+1,r,g,b)   